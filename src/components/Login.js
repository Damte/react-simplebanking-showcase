import React, { useState, useContext } from "react";
import LoginForm from "./LoginForm";
import { Context } from "./customHooks/IsAuthenticatedContext";

function LogIn() {
  const [logInDone, setLoginDone] = useContext(Context);
  const [context, setContext] = useContext(Context);
  const loginText = context.authenticated ? (
    <span>Log out</span>
  ) : (
    <span>Log in</span>
  );

  const handleChange = () => {
    setLoginDone(!logInDone);
  };
  return (
    <>
      <section id="log-in" className="log-in">
        <div className="container">
          <div className="row">
            <div className="col-md-8 offset-md-2 mb-2">
              <h1 className=" text-uppercase">{loginText}</h1>
              <LoginForm onChange={handleChange} />
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default LogIn;
