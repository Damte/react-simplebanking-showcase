import React, { useState, useEffect } from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { useHistory } from "react-router-dom";
import useDidMountEffect from "./customHooks/useDidMountEffect";

function CurrentAccount({ match }) {
  const [accountId, setAccountId] = useState(match.params.id);
  const [isLoading, setIsLoading] = useState(true);
  const [createdDateTime, setCreatedDateTime] = useState([]);
  const [updatedDateTime, setUpdatedDateTime] = useState([]);
  const [account, setAccount] = useState({});

  const [buttonClicked, setButtonClicked] = useState(false);
  const history = useHistory();

  useEffect(() => {
    fetchAccount();
  }, [buttonClicked]);

  useEffect(() => {
    fetchAccount();
  }, []);

  useDidMountEffect(() => {
    setCreatedDateTime([
      account.createdAt.slice(0, 10),
      account.createdAt.slice(11, 16),
    ]);
    setUpdatedDateTime([
      account.updatedAt.slice(0, 10),
      account.updatedAt.slice(11, 16),
    ]);
  }, [account]);

  const toggleActiveClick = (e) => {
    toggleActive();
    setButtonClicked(!buttonClicked);
  };

  const optionsJSON = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.token}`,
    },
  };

  const toggleActive = async () => {
    const data = await fetch(
      `http://localhost:8080/account/${accountId}/toggleActive`,
      optionsJSON
    );
    const json = await data.json();
  };

  const fetchAccount = async () => {
    setIsLoading(true);
    try {
      let headers = {};
      if (localStorage.token) {
        headers = {
          Authorization: `Bearer ${localStorage.token}`,
        };
      }
      const data = await fetch(`http://localhost:8080/account/${accountId}`, {
        headers: headers,
      });
      const json = await data.json();
      setAccount(json);
    } catch (error) {
      alert(error);
    }
    setIsLoading(false);
  };

  return (
    <>
      <h1 className="text-light bg-dark pb-3 pt-2 text-center">
        Account details
      </h1>
      {isLoading ? (
        <div>Loading ...</div>
      ) : (
        <Card className="offset-2 col-8 mb-3 mt-3">
          <Card.Header className="h4">
            <div className="text-muted">Account Number: </div>{" "}
            {account.accountNumber}
          </Card.Header>
          <Card.Body>
            <Card.Title>
              <span className="text-muted"> Current balance: </span>
              <span className="float-right">{account.currentBalance} EUR</span>
            </Card.Title>
            <Card.Text>
              <span className="text-capitalize">
                <span className="text-muted ">Status: </span>{" "}
                <span className="float-right">
                  {account.active ? "Active" : "Inactive"}
                </span>
              </span>
              <div className="text-capitalize">
                <span className="text-muted ">Created: </span>
                <span className="float-right">
                  {" "}
                  {`${createdDateTime[0]} at ${createdDateTime[1]}`}
                </span>
              </div>
              <div className="text-capitalize">
                <span className="text-muted ">Last modified: </span>{" "}
                <span className="float-right">
                  {" "}
                  {`${updatedDateTime[0]} at ${updatedDateTime[1]}`}
                </span>
              </div>
            </Card.Text>
            <span className="row">
              <span className="col-6 offset-3">
                <Button onClick={toggleActiveClick} variant="dark" block>
                  Change status
                </Button>
                <Button onClick={() => history.goBack()} variant="dark" block>
                  Go Back to Accounts List
                </Button>
                <Button
                  onClick={() =>
                    history.push(`${match.params.id}/transactions/`)
                  }
                  variant="dark"
                  block
                >
                  Check movements
                </Button>
              </span>
            </span>
          </Card.Body>
        </Card>
      )}
    </>
  );
}

export default CurrentAccount;
