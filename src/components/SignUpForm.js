import React, { useState, useContext } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import useForm from "./customHooks/useForm";
import validate from "./validation/SignupFormValidationRules";

function SignUpForm() {
  const [userSignUp, setUserSignUp] = useState({
    dateOfBirth: "",
    firstName: "",
    lastName: "",
    password: "",
  });
  const [signUpDone, setSignUpDone] = useState(false);
  const [personalID, setPersonalID] = useState("");

  const handleInputChange = (event) => {
    const value = event.target.value;
    setUserSignUp({
      ...userSignUp,
      [event.target.name]: value,
    });
    handleChange(event);
  };

  const optionsJSON = {
    method: "POST",
    body: JSON.stringify(userSignUp),
    headers: {
      "Content-Type": "application/json",
    },
  };

  const doSignUp = async () => {
    const data = await fetch("http://localhost:8080/register", optionsJSON);
    const json = await data.json();
    setSignUpDone(true);
    setPersonalID(json.personalId);
  };

  const { values, errors, handleChange, handleSubmit } = useForm(
    doSignUp,
    validate
  );

  const renderSignUp = signUpDone ? (
    <div className="h2 pb-3 text-light text-center">
      {" "}
      <p className="text-muted">
        Welcome{" "}
        {`${userSignUp.firstName} ${userSignUp.lastName}. Your personal Id is:`}{" "}
      </p>{" "}
      {`${personalID}`}{" "}
      <p className="h4 pt-2">
        <span className="text-muted">Please </span> write this number down{" "}
        <span className="text-muted">
          {" "}
          because you will need it in order to login to our app.
        </span>
      </p>{" "}
    </div>
  ) : (
    <div>
      <form
        className="text-dark text-capitalize text-left"
        onSubmit={handleSubmit}
        noValidate
      >
        <div className="form-group">
          <label htmlFor="firstName">First name:</label>
          <input
            id="firstName"
            type="text"
            className="form-control"
            placeholder="Enter first name"
            // value={userSignUp.firstName}
            value={values.firstName || ""}
            onChange={handleInputChange}
            name="firstName"
          ></input>
          {errors.firstName && (
            <p className="text-danger">{errors.firstName}</p>
          )}
        </div>
        <div className="form-group">
          <label htmlFor="lastNameInput">Last name:</label>
          <input
            type="text"
            id="lastNameInput"
            className="form-control"
            placeholder="Enter last name"
            value={values.lastName || ""}
            onChange={handleInputChange}
            name="lastName"
          ></input>
          {errors.lastName && <p className="text-danger">{errors.lastName}</p>}
        </div>
        <div className="form-group">
          <label htmlFor="dateInput">Birthday:</label>
          <input
            id="dateInput"
            className="form-control"
            type="date"
            placeholder="Birthday"
            value={values.dateOfBirth || ""}
            onChange={handleInputChange}
            name="dateOfBirth"
          ></input>
          {errors.dateOfBirth && (
            <p className="text-danger">{errors.dateOfBirth}</p>
          )}
        </div>
        <div className="form-group">
          <label htmlFor="passwordInput">Password:</label>
          <input
            id="passwordInput"
            className="form-control"
            type="password"
            placeholder="Password"
            value={values.password || ""}
            onChange={handleInputChange}
            name="password"
          ></input>
          {errors.password && <p className="text-danger">{errors.password}</p>}
        </div>
        <div className="text-right">
          <Button type="submit" variant="light" size="lg">
            Sign up
          </Button>
        </div>
      </form>
    </div>
  );

  return <>{renderSignUp}</>;
}

export default SignUpForm;
