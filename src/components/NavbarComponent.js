import React, { useContext } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import { Context } from "./customHooks/IsAuthenticatedContext";

function NavbarComponent() {
  const [context, setContext] = useContext(Context);

  const loginDone = !context.authenticated ? (
    <></>
  ) : (
    <>
      <Link to="/customer/accounts">
        <Nav.Link href="#accounts">Accounts</Nav.Link>
      </Link>
      <Link to="/transactions">
        <Nav.Link href="#transactions">New transaction</Nav.Link>
      </Link>
    </>
  );

  const loginDone2 = context.authenticated ? (
    <></>
  ) : (
    <Link to="/signup">
      <Nav.Link href="signout">Sign up</Nav.Link>
    </Link>
  );

  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Link to="/">
        <Navbar.Brand href="#home">Mini Back Office</Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">{loginDone}</Nav>
        <Nav>
          {loginDone2}
          <Link to="/login">
            <Nav.Link eventKey={2} href="#memes">
              Login/Logout
            </Nav.Link>
          </Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavbarComponent;
