export default function validate(values) {
  let errors = {};
  if (!values.destination) {
    errors.destination = "Account number is required";
  } else if (
    !/[A-Z]{2}\d{2}[ ]\d{4}[ ]\d{4}[ ]\d{4}/g.test(values.destination)
  ) {
    errors.destination = "XX00 0000 0000 0000 format";
  }
  if (!values.amount) {
    errors.amount = "Amount is required";
  } else if (!/^\d+$/g.test(values.amount)) {
    errors.amount = "Amount should be only numbers";
  }
  if (!values.concept) {
    errors.concept = "Concept should not be empty";
  } else if (values.concept.length > 50) {
    errors.concept = "Concept shouldnt be longer than 50 characters ";
  }
  return errors;
}
