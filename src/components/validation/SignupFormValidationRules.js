export default function validate(values) {
  let errors = {};
  if (!values.firstName) {
    errors.firstName = "First name is required";
  } else if (!/^[A-Z](\D+)\s?(\D+)?$/g.test(values.firstName)) {
    errors.firstName = "Not a valid first name";
  }
  if (!values.lastName) {
    errors.lastName = "Last name is required";
  } else if (!/^[A-Z](\D+)\s?(\D+)?$/g.test(values.lastName)) {
    errors.lastName = "Not a valid last name";
  }
  if (!values.dateOfBirth) {
    errors.dateOfBirth = "Birthday is required";
  } else if (
    !/^[12]\d{3}(\-)(((0)[0-9])|((1)[0-2]))(\-)([0-2][0-9]|(3)[0-1])$/.test(
      values.dateOfBirth
    )
  ) {
    errors.dateOfBirth = "Enter a valid date ";
  }
  if (!values.password) {
    errors.password = "Password is required";
  } else if (values.password.length < 8) {
    errors.password = "Password must be 8 or more characters";
  }
  return errors;
}
