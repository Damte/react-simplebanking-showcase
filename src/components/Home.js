import React from "react";
import image from "../images/dark.jpg";

var styles = {
  backgroundImage: "url(" + image + ")",
};

export const Home = () => (
  <div className="text-center text-light" style={styles}>
    <br />
    <br />
    <br />
    <br />
    <h1>Welcome to Mini Back Office</h1>
    <br />
    <br />
    <h1>Home Page</h1>
    <br />
    <br />
    <br />
    <br />
    <br />
  </div>
);

export default Home;
