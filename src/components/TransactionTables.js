import React, { useState, useEffect, useContext } from "react";
import Table from "react-bootstrap/Table";
import TransactionRow from "./TransactionTableRow";
import Button from "react-bootstrap/Button";
import { useHistory } from "react-router-dom";

function TableOfTransactions({ match }) {
  const [accountId, setAccountId] = useState(match.params.id);
  const [buttonClicked, setButtonClicked] = useState(false);
  const [transactions, setTransactions] = useState([]);
  const [hasLoaded, setHasLoaded] = useState(false);

  const history = useHistory();
  let displayTransactionsOrEmpty = hasLoaded ? (
    <tr>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
    </tr>
  ) : (
    transactions.map((transaction) => (
      <TransactionRow
        key={transactions.transactionId}
        tableId={transactions.indexOf(transaction) + 1}
        data={transaction}
        accountId={accountId}
      />
    ))
  );

  useEffect(() => {
    fetchTransactions();
  }, []);

  const fetchTransactions = async () => {
    let headers = {};
    if (localStorage.token) {
      headers = {
        Authorization: `Bearer ${localStorage.token}`,
      };
    }
    const data = await fetch(
      `http://localhost:8080/account/${accountId}/movements`,
      { headers: headers }
    );
    const json = await data.json();
    setTransactions(json);
  };

  return (
    <>
      <div className="pl-3 pr-3 mb-1 mt-1 bg-dark container">
        <div className="row">
          <div className="col-8 offset-md-3 mb-4">
            <h1 className="text-light text-capitalize">Movements history</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-6 offset-3 pb-4 pr-1">
            <Button
              onClick={() => history.go(-2)}
              variant="outline-light"
              size="large"
              block
            >
              Go back to accounts
            </Button>
          </div>
        </div>
      </div>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>Date</th>
            <th>Origin account</th>
            <th>Destination account</th>
            <th>Description</th>
            <th>Amount</th>
          </tr>
        </thead>

        <tbody>{displayTransactionsOrEmpty}</tbody>
      </Table>
    </>
  );
}

export default TableOfTransactions;
