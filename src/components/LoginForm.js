import React, { useState, useContext } from "react";
import Button from "react-bootstrap/Button";
import { Context } from "./customHooks/IsAuthenticatedContext";
import useForm from "./customHooks/useForm";
import { useHistory } from "react-router-dom";
import validate from "./validation/LoginFormValidationRules";

function LoginForm(props) {
  const [userLogin, setUserLogin] = useState({
    personalId: "",
    password: "",
  });
  const [jwt, setJwt] = useState({});
  const [id, setId] = useState(0);
  const [logInDone, setLogInDone] = useState(false);
  const [context, setContext] = useContext(Context);
  const [initialValues, setInitialValues] = useState({
    personalId: "",
    password: "",
  });

  const handleInputChange = (event) => {
    const value = event.target.value;
    setUserLogin({
      ...userLogin,
      [event.target.name]: value,
    });

    handleChange(event);
  };

  const optionsJSON = {
    method: "POST",
    body: JSON.stringify(userLogin),
    headers: {
      "Content-Type": "application/json",
    },
  };
  const logOut = () => {
    alert("You have logged out");
    localStorage.clear();
    setLogInDone(false);
    props.onChange();
    setContext({ ...context, authenticated: false, customerId: 0 });
  };

  const history = useHistory();
  const logIn = async () => {
    const data = await fetch("http://localhost:8080/authenticate", optionsJSON);
    const json = await data.json();
    let toki = json.token;
    let customerId = json.userId;
    setJwt(toki);
    setId(customerId);

    localStorage.setItem("token", toki);
    localStorage.setItem("customerId", customerId);
    setLogInDone(true);
    props.onChange();
    if (toki) {
      setContext({ ...context, authenticated: true, customerId: customerId });
    }

    setUserLogin(initialValues);
    history.push(`/customer/accounts`)
  };

  const { values, errors, handleChange, handleSubmit } = useForm(
    logIn,
    validate
  );

  const toggleLoginLogout = context.authenticated ? (
    <div>
      <Button
        onClick={() => {
          logOut();
        }}
        variant="outline-dark"
        size="lg"
      >
        Log out
      </Button>
    </div>
  ) : (
    <div>
      <form
        className="text-dark text-capitalize text-left"
        onSubmit={handleSubmit}
        noValidate
      >
        <div className="form-group">
          <label htmlFor="personalId">Personal Id:</label>
          <input
            id="personalId"
            className="form-control"
            type="text"
            placeholder="Enter personal Id"
            value={values.personalId || ""}
            onChange={handleInputChange}
            name="personalId"
          ></input>
          {errors.personalId && (
            <p className="text-danger">{errors.personalId}</p>
          )}
        </div>
        <div className="form-group">
          <label htmlFor="passwordInput">Password:</label>
          <input
            id="passwordInput"
            className="form-control"
            type="password"
            placeholder="Password"
            value={values.password || ""}
            onChange={handleInputChange}
            name="password"
          ></input>
          {errors.password && <p className="text-danger">{errors.password}</p>}
        </div>
        <div className="text-right">
          <Button type="submit" variant="outline-dark" size="lg">
            Log in
          </Button>
        </div>
      </form>
    </div>
  );

  return <>{toggleLoginLogout}</>;
}

export default LoginForm;
