import React from "react";
import {
  FaFacebook,
  FaGithub,
  FaGoogle,
  FaInstagram,
  FaLinkedin,
  FaTwitter,
} from "react-icons/fa";

function Footer() {
  return (
    <>
      <footer className="bg-dark text-center text-lg-start text-white">
        <div className="container p-4">
          <section className="mb-4">
            <a
              className="btn btn-outline-light btn-floating m-1"
              style={{ backgroundColor: "#4a54f1" }}
              href="https://www.facebook.com"
              role="button"
            >
              <i>
                <FaFacebook />
              </i>
            </a>

            <a
              className="btn btn-outline-light btn-floating m-1"
              style={{ backgroundColor: "#55acee" }}
              href="https://www.twitter.com"
              role="button"
            >
              <i>
                <FaTwitter />
              </i>
            </a>

            <a
              className="btn btn-outline-light btn-floating m-1"
              style={{ backgroundColor: "#dd4b39" }}
              href="https://www.google.ee"
              role="button"
            >
              <i>
                <FaGoogle />
              </i>
            </a>

            <a
              className="btn btn-outline-light btn-floating m-1"
              style={{ backgroundColor: "#ac2bac" }}
              href="https://www.instagram.com"
              role="button"
            >
              <i>
                <FaInstagram />
              </i>
            </a>

            <a
              className="btn btn-outline-light btn-floating m-1"
              style={{ backgroundColor: "#0082ca" }}
              href="https://www.linkedin.com"
              role="button"
            >
              <i>
                <FaLinkedin />
              </i>
            </a>
            <a
              className="btn btn-outline-light btn-floating m-1"
              style={{ backgroundColor: "#333333" }}
              href="https://www.github.com"
              role="button"
            >
              <i>
                <FaGithub />
              </i>
            </a>
            <div></div>
          </section>

          <section className="mb-4">
            <p className="mb-4 text-muted offset-2 col-8">
              Search and view detailed information of your accounts and their
              payments/transactions.
            </p>
            <p className="mb-4 text-muted offset-2 col-8">
              Make transactions from your account. Anytime anywhere.
            </p>
          </section>
        </div>

        <div
          className="text-center p-3"
          style={{ backgroundColor: (0, 0, 0, 0.2) }}
        >
          © 2021 Copyright:
          <a className="text-white" href="https://mdbootstrap.com/">
            SimpleBankApp.com
          </a>
        </div>
      </footer>
    </>
  );
}

export default Footer;
