export default function validate(values) {
  let errors = {};
  if (!values.personalId) {
    errors.personalId = "Personal id is required";
  } else if (values.personalId.length !== 10) {
    errors.personalId = "Personal id should have 10 digits";
  }
  if (!values.password) {
    errors.password = "Password is required";
  } else if (values.password.length < 8) {
    errors.password = "Password must be 8 or more characters";
  }
  return errors;
}
