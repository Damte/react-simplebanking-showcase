import React, { useState } from "react";
import { useHistory } from "react-router-dom";

function TableRow({ triggerRerender, keyValue, data }) {
  const [buttonClicked, setButtonClicked] = useState(false);
  const [isActive, setIsActive] = useState(true);
  const [selectedAccount, setSelectedAccount] = useState(data.id);

  const history = useHistory();
  const handleClick = (e) => {
    history.push(`accounts/${data.id}`);
    setIsActive(!isActive);
  };
  const handleClickButton = (e) => {
    history.push(`${data.id}`);
    setIsActive(!isActive);
  };
  const toggleActive = (e) => {
    if (data.active === true) {
      makeInactive();
    } else {
      makeActive();
    }
    setButtonClicked(!buttonClicked);
    triggerRerender(selectedAccount);
  };

  const optionsJSON = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.token}`,
    },
  };

  const makeInactive = async () => {
    const data = await fetch(
      `http://localhost:8080/account/${selectedAccount}/activate`,
      optionsJSON
    );
    const json = await data.json();
  };
  const makeActive = async () => {
    const data = await fetch(
      `http://localhost:8080/account/${selectedAccount}/activate`,
      optionsJSON
    );
    const json = await data.json();
  };

  return (
    <>
      <tr onClick={handleClick}>
        <td>{keyValue}</td>
        <td>{data.accountNumber}</td>
        <td className="text-right">{data.currentBalance}</td>
        <td className="text-right">
          {data.active ? <span>Active</span> : <span>Inactive</span>}
        </td>
        <td>{data.createdAt.slice(0, 10)}</td>
      </tr>
    </>
  );
}
export default TableRow;
