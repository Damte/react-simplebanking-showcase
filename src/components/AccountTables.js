import React, { useState, useEffect, useContext } from "react";
import TableRow from "./TableRow";
import Table from "react-bootstrap/Table";
import { Context } from "./customHooks/IsAuthenticatedContext";
import Button from "react-bootstrap/Button";

function TableOfAccounts() {
  const [context, setContext] = useContext(Context);

  const [customerId, setCustomerId] = useState(localStorage.customerId);
  const [customerFullName, setCustomerFullName] = useState("");

  const [buttonClicked, setButtonClicked] = useState(false);
  const [accounts, setAccounts] = useState([
    {
      createdAt: "",
      updatedAt: "",
      active: "",
      id: "",
      accountNumber: "",
      currentBalance: "",
    },
  ]);

  useEffect(() => {
    setCustomerId(localStorage.customerId);
    fetchCustomer();
    fetchAccounts();
  }, [customerId]);
  useEffect(() => {
    fetchAccounts();
  }, [buttonClicked]);

  function triggerRerender(value) {
    setButtonClicked(!buttonClicked);
  }

  const fetchCustomer = async () => {
    let headers = {};
    if (localStorage.token) {
      headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.token}`,
      };
    }

    const data = await fetch(`http://localhost:8080/customer/${customerId}`, {
      headers: headers,
    });
    const json = await data.json();
    setCustomerFullName(`${json.firstName} ${json.lastName}`);
  };

  const fetchAccounts = async () => {
    let headers = {};
    if (localStorage.token) {
      headers = {
        Authorization: `Bearer ${localStorage.token}`,
      };
    }
    const data = await fetch(
      `http://localhost:8080/customer/${customerId}/accounts`,
      { headers: headers }
    );
    const json = await data.json();
    setAccounts(json);
  };

  const optionsJSON = {
    method: "POST",
    body: JSON.stringify(customerId),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.token}`,
    },
  };

  const createAccount = async () => {
    const data = await fetch(`http://localhost:8080/account/`, optionsJSON);
    const json = await data.json();
    setButtonClicked(!buttonClicked);
  };

  return (
    <>
      <div className="pl-3 pr-3 mb-1 mt-1 container">
        <span className="row bg-dark">
          <h1 className="text-light bg-dark col-8 pb-3 pt-3 ml-4">
            Welcome {customerFullName}
          </h1>
          <span className="float-right col-3 pt-4 pr-1">
            <Button
              onClick={createAccount}
              variant="outline-light"
              size="large"
              block
            >
              Create account
            </Button>
          </span>
        </span>
        <div className="row bg-dark">
          <p className="offset-0 col-10 text-left pb-1 ml-4">
            <small className="text-light text-muted">
              Click on the row for seeing more details about one specific
              account
            </small>
          </p>
        </div>
      </div>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th className="col-1">#</th>
            <th className="col-4">Account number</th>
            <th className="col-3 text-right">Balance</th>
            <th className="col-3 text-right">Status</th>
            <th className="col-1 text-right">Created</th>
          </tr>
        </thead>

        <tbody>
          {accounts.map((account) => (
            <TableRow
              key={accounts.indexOf(account) + 1}
              triggerRerender={triggerRerender}
              keyValue={accounts.indexOf(account) + 1}
              data={account}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export default TableOfAccounts;
