import React, { useState } from "react";

function TransactionRow({ tableId, data, accountId }) {
  const [buttonClicked, setButtonClicked] = useState(false);
  const [isActive, setIsActive] = useState(true);

  const date = data.transactionDateTime.slice(0, 10);
  const time = data.transactionDateTime.slice(11, 16);

  const signum = data.originAccountId == accountId ? "-" : "+";

  return (
    <>
      <tr>
        <td>{tableId}</td>
        <td>{`${date} at ${time}`}</td>

        <td>{data.originAccountNumber}</td>
        <td>{data.destinationAccountNumber}</td>
        <td>{data.description}</td>
        <td>
          {signum}
          {data.transactionAmount}
        </td>
      </tr>
    </>
  );
}
export default TransactionRow;
