In the project directory, you can run:

### `npm start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The corresponding backend for running this project has been uploaded into the following repository: https://bitbucket.org/Damte/showcase-simplebankapp/src/master/

* After running the backend a new user must be created in order to run the app. 
* In order to do that the user must go to the url: http://localhost:3000/signup or navigate toward there in the navigation bar on the top. 
* After successfuly completing the sign up then a personal id would be automatically generated and showed to us in the screen. This personal id it is necessary to login, along with the chosen password.
* New users will get automatically an active account (that by default will include 50 EUR in order to be able to try to do some transfers). Also other accounts can be added and they currently will get still the same starting amount.
* After Login, the app will navigate to the main account page, where the user can see a list with their accounts.
* From there it can create new accounts or navigate toward one of the account details by clicking the row.
* Once inside the account details, the customer can see some more details about the account and see/edit the current state (active/inactive) of the account.
* From there, by clicking a button it will navigate to a view displaying the movements of the account. For a new user, it will be empty.
* In order to add something in there we shall create a new transaction. This can be done by clicking the "New transaction" tab on the Navbar.
* By clicking the buttom submit there will be a number of validations of the fields in the frontend and also the backend will check some set conditions (fe, that the destination account exist, that there has not been made more than 5 transfers this day, that you are not atempting 2 transfers in less than 1 minute(to avoid that the user unadvertedly try to do the same transwer twice), etc.)
* Finally the user can log out what will cause the jwt to be deleted.
* The input validation is currently existing for Sign up, log in and transfer, but in some aspects is quite basic, in the future more complex validation expressions that would cover more ground. Currently, the user needs to introduce the account number with spaces in XX00 0000 0000 0000 format, in the future this either will be enforced by a more customised submit type or it will try to parse some alternatives formats for the string after receiving it.
