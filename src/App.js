import './App.css';
import React, { useState, useEffect } from 'react'
import {Home} from './components/Home';
import NavbarComponent from './components/NavbarComponent';
import AccountTables from './components/AccountTables';
import Transaction from './components/NewTransaction';
import TableOfTransactions from './components/TransactionTables';
import Login from './components/Login'
import Signup from './components/SignUp'
import Footer from './components/Footer'
import CurrentAccount from './components/CurrentAccount'
import { Context } from "./components/customHooks/IsAuthenticatedContext";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'


function App() {
  const [context, setContext] = useState(
    {
      authenticated: false,
      customerId: 0
    }
  );
  useEffect(() => {
  if (localStorage.length > 0) {
    setContext({...context, authenticated: true, customerId: localStorage.customerId})
  }
  }, [])



  return (
    <div className="p-3 mb-2 container" >
      <Router>
        <Context.Provider value={[context, setContext]}>       
        <NavbarComponent />
          <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/signup" component={Signup} />
          <Route path="/login" component={Login} />
            <Route path="/customer/accounts" exact component={AccountTables} />
            <Route path="/customer/accounts/:id" exact component={CurrentAccount}/>
            <Route path="/customer/accounts/:id/transactions" component={TableOfTransactions}/>
          <Route path="/transactions" component={Transaction} />
            </Switch>

        <Footer/>
        </Context.Provider>
        </Router>
    </div>
  );
}

export default App;
